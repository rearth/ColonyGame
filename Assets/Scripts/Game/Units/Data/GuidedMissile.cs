﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuidedMissile : MonoBehaviour {
    public Vector3 targetPos = Vector3.zero;
    public GameObject explosionPrefab;

    private Vector3 startPos = Vector3.zero;
    private float totalDistance;
    private float totalTime;
    private float timePassed = 0;

    private void FixedUpdate() {
        if (targetPos == Vector3.zero) {
            return;
        }

        if (startPos == Vector3.zero) {
            startPos = this.transform.position;
            totalDistance = Vector3.Distance(startPos, targetPos);
            totalTime = totalDistance / 20;
        }

        timePassed += Time.deltaTime;
        var progress = timePassed / totalTime;
        var height = 6.608947 * progress - 12.76575 * Mathf.Pow(progress, 2) + 6.156806 * Mathf.Pow(progress, 3);
        height *= (totalDistance / 3.5f);
        var dir = targetPos - startPos;
        dir *= progress;
        dir.y += (float) height;
        var rot = startPos + dir - this.transform.position;
        Transform transform1;
        (transform1 = this.transform).rotation = Quaternion.LookRotation(rot);
        transform1.Rotate(new Vector3(-90f, 0, 0));
        transform1.position = startPos + dir;
    }

    private void OnTriggerEnter(Collider other) {
        Debug.Log("Guided missile exploding!");
        
        GameObject.Instantiate(explosionPrefab, this.transform.position, Quaternion.identity);
        GameObject.Destroy(this.gameObject);
    }
}