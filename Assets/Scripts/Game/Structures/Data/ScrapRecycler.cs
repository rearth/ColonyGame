﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrapRecycler : ProductionStructure {
	public override List<ProduceData> getProduceData() {
		var list = new List<ProduceData> {
			new ProduceData() {
				consume = new List<ressourceStack>() {new ressourceStack(5, ressources.Scrap)},
				outcome = new List<ressourceStack>() {
					new ressourceStack(2.5f, ressources.Stone),
					new ressourceStack(1f, ressources.Wood),
					new ressourceStack(0.3f, ressources.Iron),
				},
				energyCost = 2f
			}
		};
		return list;
	}
}
