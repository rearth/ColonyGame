﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NukeSilo : DefaultStructure {

	public Sprite launchBut;
	public GameObject missilePrefab;
	
	private GameObject cancelBut = null;
	private Vector3 target;

	public override int getMaxEnergy() {
		return 1000;
	}

	public override int getMaxInput() {
		return 50;
	}

	public override int getMaxOutput() {
		return 0;
	}

	public override PopUpCanvas.popUpOption[] getOptions() {
		PopUpCanvas.popUpOption info = new PopUpCanvas.popUpOption("Info", infoBut);
		PopUpCanvas.popUpOption launch = new PopUpCanvas.popUpOption("doLaunch", launchBut);

		if (this.getCurEnergy() < 500 || this.getInv().getAmount(ressources.Warhead) < 1) {
			launch.setEnabled(false);
		}

		var options = new[] {info, launch};
		return options;
	}

	public override int getPriority() {
		return 3;
	}

	public override void handleOption(string option) {
		Debug.Log("handling option: " + option);

		if (salvaging) {
			return;
		}

		switch (option) {
			case "doLaunch":
				Debug.Log("Clicked launch button");
				selectPoint();
				break;
			default:
				displayInfo();
				break;
		}
	}

	private void selectPoint() {
		Debug.Log("starting point selection");

		if (cancelBut == null) {
			cancelBut = GameObject.Find("Terrain").GetComponent<Scene_Controller>().NukeCancelBut;
		}
		
		Scene_Controller.getInstance().hideAllUI();
		cancelBut.SetActive(true);
		cancelBut.GetComponent<Button>().onClick.AddListener(cancel);
			
		GameObject.Find("Main Camera").GetComponent<clickDetector>().setNextRayClickAction(handleRay);
		TimeScaleHandler.setScale(0.1f);
	}
		
	private void handleRay(Ray ray) {
		print("handling ray for nuke creation: " + ray);
		RaycastHit raycastHit;

		if (!Physics.Raycast(ray, out raycastHit, 200.0f)) {
			print("raycast returned invalid!");
			return;
		}

		var hitPos = raycastHit.point;
		selectTarget(hitPos);
	}

	private void selectTarget(Vector3 target) {
		print("selected target: " + target);
		restoreNormal();
		Notification.createNotification(target, Notification.sprites.Targeting);
		this.target = target;
		this.GetComponent<Animator>().SetTrigger("work");
	}

	private void launchMissile() {
		print("launching missile");

		var missile = GameObject.Instantiate(missilePrefab, this.transform.position + new Vector3(0, 7, 0),
			Quaternion.identity);
		missile.GetComponent<GuidedMissile>().targetPos = target;
	}

	private void restoreNormal() {
		clickDetector.overlayClicked = true;
		GameObject.Find("Main Camera").GetComponent<clickDetector>().resetNextClick();
		cancelBut.GetComponent<Button>().onClick.RemoveAllListeners();
		
		TimeScaleHandler.normalizeScale();
		Scene_Controller.getInstance().restoreDefaultUI();
		cancelBut.SetActive(false);
	}

	public void cancel() {
		restoreNormal();
	}

	public override void FixedUpdate() {
		base.FixedUpdate();
		if (counter % 600 == 1 &&  this.getInv().getAmount(ressources.Warhead) < 1) {
			RessourceHelper.deliverFromAnywhere(this.gameObject, false, ressources.Warhead);
		}
	}
}
