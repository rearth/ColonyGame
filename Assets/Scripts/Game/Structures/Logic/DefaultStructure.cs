﻿using System;
using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;

public abstract class
    DefaultStructure : MonoBehaviour, EnergyContainer, Structure, clickable, SaveLoad.SerializableInfo, HPHandler.IDestroyAction {
    public ressourceStack[] ownResource = new ressourceStack[2];

    public Sprite infoBut;
    public List<GameObject> pipeObjects = new List<GameObject>();
    public bool busy = false;
    public int counter = 0;
    public float storedEnergy = 0;

    public bool salvaging = false;
    private float salvageStartHP = 100f;

    private inventory inv;

    // Use this for initialization
    public virtual void Start() {
        this.inv = this.GetComponent<inventory>();
        register();
    }

    public virtual void FixedUpdate() {
        if (this.salvaging && this.getHP().HP < 3) {
            print("structure salvaged!");
            var pickup = Instantiate(GameObject.Find("Terrain").GetComponent<Scene_Controller>().pickupBox,
                this.transform.position, Quaternion.identity);
            pickup.GetComponent<inventory>().add(new ressourceStack(salvageStartHP, ressources.Scrap));
            getHP().destruct();
        }

        if (salvaging) {
            this.getHP().HP -= 2.5f;
            return;
        }

        counter++;
        if (counter >= 5000) {
            counter = 0;
        }
    }

    public bool isWorking() {
        return !salvaging;
    }

    public virtual ressourceStack[] getCost() {
        return findDefinedPrice().ToArray();
    }

    private List<ressourceStack> findDefinedPrice() {
        var prefabs = GameObject.Find("Terrain").GetComponent<BuildingManager>().autoPrefabs;
        var res = new List<ressourceStack>();

        foreach (var elem in prefabs) {
            if (this.gameObject.name.Equals(elem.prefab.name)) {
                foreach (var stack in elem.cost) {
                    res.Add(stack.clone());
                }
            }
        }

        return res;
    }

    public ressourceStack[] getResources() {
        return this.ownResource;
    }

    public HPHandler getHP() {
        return this.GetComponent<HPHandler>();
    }

    public inventory getInv() {
        return inv;
    }

    abstract public int getMaxEnergy(); /* {
        return 2000;
    }*/

    public int getCurEnergy() {
        return (int) storedEnergy;
    }

    abstract public int getMaxOutput();

    abstract public int getMaxInput(); /* {
        if (this.getMaxEnergy() - this.getCurEnergy() < 50) {
            return this.getMaxEnergy() - this.getCurEnergy();
        }
        return 50;
    }*/

    public GameObject GetGameObject() {
        return this.gameObject;
    }

    public List<GameObject> getPipes() {
        return pipeObjects;
    }

    public void clearPipes() {
        //delete all existing pipes
        foreach (GameObject pipe in pipeObjects) {
            Debug.Log("deleting pipe: " + pipe);
            Destroy(pipe);
        }

        pipeObjects.Clear();
    }

    private void sortListByPriority(List<GameObject> list) {
        for (int i = 0; i < list.Count; i++) {
            bool changed = false;
            for (int j = i; j < list.Count - 1; j++) {
                EnergyContainer contA = list[j].GetComponent<EnergyContainer>();
                EnergyContainer contB = list[j + 1].GetComponent<EnergyContainer>();
                if (contA == null || contB == null) {
                    continue;
                }

                if (Vector3.Distance(contB.GetGameObject().transform.position, this.transform.position) <
                    Vector3.Distance(contA.GetGameObject().transform.position, this.transform.position)) {
                    changed = true;
                    list.Remove(list[j + 1]);
                    list.Insert(j, contB.GetGameObject());
                    Debug.Log("changed list position of " + contA.GetGameObject().name + " and " +
                              contB.GetGameObject().name);
                }
            }

            if (!changed) {
                break;
            }
        }
    }

    abstract public int getPriority();

        /*foreach (EnergyContainer connection in connections) {
            if (connection.getMaxInput() < 1 || connection.Equals(recievedFromLast)) {
                continue;
            }

            if (connection.getCurEnergy() < this.getCurEnergy() || connection.getPriority() > this.getPriority()) {

                if (this.getCurEnergy() - connection.getCurEnergy() < this.getMaxOutput()) {
                    continue;
                }

                float transferAmount = (connection.getMaxInput() > this.getMaxOutput()) ? connection.getMaxInput() : this.getMaxOutput();
                if (transferAmount > this.getCurEnergy()) {
                    transferAmount = this.getCurEnergy();
                }

                connection.addEnergy(transferAmount, this);
                this.addEnergy(-transferAmount, this);
            }
        }*/

    public void addEnergy(float amount, EnergyContainer from) {
        this.storedEnergy += amount;
    }

    public void register() {
        EnergyHandler.allContainers.Add(this);
        GameObject.Find("Terrain").GetComponent<EnergyHandler>().reload();
    }

    public void deregister() {
        EnergyHandler.allContainers.Remove(this);
    }

    public virtual void handleClick() {
        Debug.Log("clicked structure");

        if (salvaging)
            return;

        if (Salvaging.isActive()) {
            Salvaging.createNotification(this.gameObject);
            return;
        }
    }

    public void handleLongClick() {
        Debug.Log("long clicked structure");

        if (salvaging)
            return;

        this.GetComponent<ClickOptions>().Create(); //TODO
    }

    public void displayInfo() {
        InfoClicked controller = InfoClicked.getInstance();
        controller.show();

        controller.setTitle(this.gameObject.name);
        controller.setDesc(getDesc());
    }

    public virtual string getDesc() {
        return Environment.NewLine
               + "Kind: " + this.GetComponent<HPHandler>().niceText();
    }

    public abstract PopUpCanvas.popUpOption[] getOptions();
    /*{
         PopUpCanvas.popUpOption[] options;

         PopUpCanvas.popUpOption info = new  PopUpCanvas.popUpOption("Info", infoBut);
         //PopUpCanvas.popUpOption goTo = new  PopUpCanvas.popUpOption("DoClone", cloneBut);
        
        options = new PopUpCanvas.popUpOption[]{info};
        return options;
    }*/

    public abstract void handleOption(string option);
    /*{
        Debug.Log("handling option: " + option);

        switch (option) {
            default:
                displayInfo();
                break;
        }
    }*/

    public new string ToString {
        get { return this.gameObject.name; }
    }

    public virtual SaveLoad.SerializationInfo getSerialize() {
        return new serializationData(storedEnergy, busy, salvaging, ownResource);
    }

    public virtual void handleDeserialization(SaveLoad.SerializationInfo info) {
        serializationData data = (serializationData) info;
        this.storedEnergy = data.storedEnergy;
        this.busy = data.busy;
        this.ownResource = data.ownResource;
        this.salvaging = data.salvaging;

        if (salvaging) {
            Debug.Log("got salvaging info, creating particles....");
            //TODO, this gets called before terrain scripts are loaded
            //GameObject.Instantiate(ParticleHelper.getInstance().salvageParticles, this.gameObject.transform);
        }

        //reloadConnections();
    }

    public GameObject getGameobject() {
        return this.gameObject;
    }

    public void salvage() {
        Debug.Log("Got salvage request!");
        this.salvaging = true;
        Salvaging.displayIndicator(this.gameObject);
        this.salvageStartHP = getHP().HP;
    }

    public bool isSalvaging() {
        return salvaging;
    }

    public bool isBusy() {
        return isSalvaging() || this.busy;
    }

    [System.Serializable]
    public class serializationData : SaveLoad.SerializationInfo {
        public float storedEnergy;
        public bool busy;
        public bool salvaging;
        public ressourceStack[] ownResource;

        public serializationData(float storedEnergy, bool busy, bool salvaging, ressourceStack[] ownResource) {
            this.storedEnergy = storedEnergy;
            this.busy = busy;
            this.ownResource = ownResource;
            this.salvaging = salvaging;
        }
        
        public override string scriptTarget {
            get { return "DefaultStructure"; }
        }
    }

    public void beforeDestroy() {
        deregister();
    }
}