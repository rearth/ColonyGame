﻿using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;

public class RessourceHelper : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void deliverTo(GameObject to, bool important, ressources kind) {

        print("got deliver request: to=" + to + " important=" + important + " kind=" + kind);

        //get closest worker
        GameObject[] movers = GameObject.FindGameObjectsWithTag("mover");
        Transform mover = harvestableRessource.GetClosestMover(movers, !important, to.transform);

        //stop if no worker has been found
        if (mover == null) {
            print("no mover found!");
            return;
        }

        //find container with the ressource kind
        var results = GameObject.FindGameObjectsWithTag("dropBase");
        var minDist = float.MaxValue;
        GameObject min = null;

        foreach (var obj in results) {
            //check if res has kind, if true then order worker to deliver it and stop
            if (obj.GetComponent<inventory>().getAmount(kind) > 1) {
                if (min == null || Vector3.Distance(to.transform.position, obj.transform.position) < minDist) {
                    min = obj;
                    minDist = Vector3.Distance(to.transform.position, obj.transform.position);
                }
            }
        }

        if (min != null) {
            var stack = new ressourceStack(min.GetComponent<inventory>().getAmount(kind), kind);
            mover.GetComponent<ActionController>().deliverTo(min, to, stack);
            print("handled delivery request successfully");
            return;
        }

        print("unable to handle request!");
    }

    public static void deliverFromAnywhere(GameObject to, bool important, ressources kind) {

        print("got deliver request: to=" + to + " important=" + important + " kind=" + kind);

        //get closest worker
        GameObject[] movers = GameObject.FindGameObjectsWithTag("mover");
        Transform mover = harvestableRessource.GetClosestMover(movers, !important, to.transform);

        //stop if no worker has been found
        if (mover == null) {
            print("no mover found!");
            return;
        }

        //find container with the ressource kind
        var results = GameObject.FindObjectsOfType<inventory>();

        foreach (var obj in results) {

            if (obj.CompareTag("mover")) continue;  //do not use movers, only accept other things
            if (obj.gameObject.Equals(to)) continue;

            //check if res has kind, if true then order worker to deliver it and stop
            var Inv = obj.GetComponent<inventory>();
            if (Inv.getAmount(kind) > 0) {
                var amount = Inv.getAmount(kind);
                var toInv = to.GetComponent<inventory>();
                if (!toInv.canTake(kind, amount)) {
                    amount = toInv.getFreeSpace();
                }
                var stack = new ressourceStack(amount, kind);
                mover.GetComponent<ActionController>().deliverTo(obj.gameObject, to, stack);
                print("handled delivery request successfully");
                return;
            }
        }

        print("unable to handle request!");
    }

    public static GameObject getClosestDropbase(Vector3 point) {
        
        var results = GameObject.FindGameObjectsWithTag("dropBase");
        var minDist = float.MaxValue;
        GameObject min = null;

        foreach (var obj in results) {
            //check if res has kind, if true then order worker to deliver it and stop
            var distance = Vector3.Distance(point, obj.transform.position);
            if (min == null || distance < minDist) {
                min = obj;
                minDist = distance;
            }
        }

        return min;
    }

    public static void pickupFrom(GameObject from, ressources kind) {
        var target = getClosestDropbase(from.transform.position);
        //get closest worker
        GameObject[] movers = GameObject.FindGameObjectsWithTag("mover");
        Transform mover = harvestableRessource.GetClosestMover(movers, false, target.transform);

        //stop if no worker has been found
        if (mover == null) {
            print("no mover found!");
            return;
        }
        
        var stack = new ressourceStack(from.GetComponent<inventory>().getAmount(kind), kind);
        mover.GetComponent<ActionController>().deliverTo(from, target.gameObject, stack);
        print("handled pickup request successfully");
    }

    public static void deliverTo(GameObject to, bool important, List<ressourceStack> currentlyProducingCost) {
        foreach (var elem in currentlyProducingCost) {
            deliverTo(to, important, elem.getRessource());
        }
    }
}
