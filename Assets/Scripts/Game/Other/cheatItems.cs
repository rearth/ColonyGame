﻿using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;

public class cheatItems : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.GetComponent<inventory>().add(new ressourceStack(3000, ressources.Wood));
		this.GetComponent<inventory>().add(new ressourceStack(3000, ressources.Stone));
		this.GetComponent<inventory>().add(new ressourceStack(300, ressources.OreIron));
		this.GetComponent<inventory>().add(new ressourceStack(300, ressources.Gold));
		this.GetComponent<inventory>().add(new ressourceStack(2000, ressources.Iron));
		this.GetComponent<inventory>().add(new ressourceStack(3000, ressources.Water));
		this.GetComponent<inventory>().add(new ressourceStack(200, ressources.Uranium));
		this.GetComponent<inventory>().add(new ressourceStack(1000, ressources.Scrap));
		this.GetComponent<inventory>().add(new ressourceStack(10, ressources.Thorium));
		this.GetComponent<inventory>().add(new ressourceStack(30, ressources.Electrum));
		this.GetComponent<inventory>().add(new ressourceStack(30, ressources.DepletedUranium));
		this.GetComponent<inventory>().add(new ressourceStack(10, ressources.Warhead));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
