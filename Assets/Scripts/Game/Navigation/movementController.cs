﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class movementController : MonoBehaviour, SaveLoad.SerializableInfo {
    
    private Vector3 target = Vector3.zero;
    private Transform targetObject;
    public GameObject trailEmitter;
    public NavMeshAgent agent;
    private Animator animator;
    private ActionController actionController;

    // Use this for initialization
	void Start () {
        actionController = GetComponent<ActionController>();
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    void Update() {

        animator.SetFloat("moveSpeed", agent.velocity.magnitude);

        if (target == Vector3.zero) {
            return;
        }

        agent.SetDestination(target);
        
        if (agent.remainingDistance < 0.5f && agent.hasPath) {
            target = Vector3.zero;
            agent.isStopped = true;

            actionController.handleTarget(targetObject);
        }
    }

    public void moveRand() {
        Vector3 goTo = this.transform.forward * UnityEngine.Random.Range(1, 5) + this.transform.right * UnityEngine.Random.Range(-3f, 3f);
        moveTo(goTo + this.transform.position, false);
    }

    public void moveTo(Vector3 target, bool displayTarget = true) {

        if (agent == null) {
            Start();
        }
        
        this.target = target;
        agent.SetDestination(this.target);
        if (displayTarget) {
            displayPath(this.target, "move");
        }
        agent.isStopped = false;

        actionController.stop();
        if (this.target == Vector3.zero) {
            Debug.Log("Error while setting target");
        }
    }

    public void setTarget(Transform target) {
        this.setTarget(target, "move");
    }

    public void setTarget(Transform target, string mode) {
        this.targetObject = target;

        Vector3 closestPoint = target.gameObject.GetComponent<Collider>().ClosestPoint(this.transform.position);

        if (Vector3.Distance(closestPoint, this.transform.position) <= 0.5f) {
            this.target = Vector3.zero;
            agent.isStopped = true;

            actionController.handleTarget(targetObject);
            return;
        }

        //closestPoint.y = target.transform.position.y;

        /*NavMeshHit hit;
        bool blocked = NavMesh.SamplePosition(closestPoint, out hit, 20.0f, NavMesh.AllAreas);

        if (blocked) {
            this.target = hit.position;
        } else {
            this.target = closestPoint;
        }*/
        this.target = closestPoint;
        agent.SetDestination(this.target);
        displayPath(this.target, mode);
        agent.isStopped = false;

        actionController.stop();

        if (this.target == Vector3.zero) {
            Debug.Log("Error while setting target");
        }
    }

    private void displayPath(Vector3 target, string mode) {

        if (Vector3.Distance(transform.position, target) < 1.0f) {
            return;
        }

        Vector3 spawnAt = transform.position;
        spawnAt.y += 1f;
        Vector3 targetAt = target;
        //targetAt.y += 2f;

        GameObject emitter = Instantiate(trailEmitter, spawnAt, transform.rotation);
        emitter.GetComponent<TrailPathMover>().setPath(targetAt);
        emitter.GetComponent<TrailPathMover>().setColor(mode);
    }

    public Transform getClosest(string tag) {
        GameObject[] targets = GameObject.FindGameObjectsWithTag(tag);

        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;

        foreach(GameObject potentialTarget in targets) {
            
            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if(dSqrToTarget < closestDistanceSqr) {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget.transform;
            }
        }
     
        return bestTarget;
    }
    
    public SaveLoad.SerializationInfo getSerialize() {
        return new serializationData(target, targetObject);
    }

    public void handleDeserialization(SaveLoad.SerializationInfo info) {

        serializationData data = (serializationData) info;
        this.target = data.target;
        try {
            this.targetObject = atPos(data.targetObject).transform;
        } catch (NullReferenceException ex) {
            this.targetObject = null;
        }
    }

    [System.Serializable]
    class serializationData : SaveLoad.SerializationInfo {
        public SaveLoad.SerializableVector3 target;
        public SaveLoad.SerializableVector3 targetObject;

        public serializationData(Vector3 target, Transform targetObject) {
            this.target = target;

            try {
                this.targetObject = targetObject.position;
            } catch (Exception ex) {
                this.targetObject = Vector3.zero;
            }
        }

        public override string scriptTarget {
            get {
                return "movementController";
            }
        }
    }
    private GameObject atPos(Vector3 position) {
        return ActionController.atPos(position);
    }
}
