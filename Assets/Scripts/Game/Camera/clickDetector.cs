﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class clickDetector : MonoBehaviour {

    private float onStart;
    private float lastClick;
    public static bool overlayClicked;
    public GameObject ClickableInfo;

    //new int, used to display the number of active overlays/menus, click detection is only happening when this equals 0.
    //increment when opening a menu, decrement when closing
    public static int menusOpened = 0;

    private static List<Outline> outlines = new List<Outline>();

    private Action<GameObject> nextAction;
    private Action<Ray> nextRayAction;
    private Camera camera;
    Vector3 start = Vector3.zero;

    // Update is called once per frame
    private void Start() {
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    void Update() {

        if (Building.buildingMode || ClickableInfo.activeSelf || ResourceDisplay.menuOpened || menusOpened > 0) {
            return;
        }

        if (((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began)) || Input.GetMouseButtonDown(0)) {
            onStart = Time.unscaledTime;
        }

        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Ended)) {

            var sinceLast = Time.unscaledTime - lastClick;
            lastClick = Time.unscaledTime;

            if (Input.GetTouch(0).deltaPosition.magnitude > 3.0f || overlayClicked || sinceLast < 0.15) {
                overlayClicked = false;
                return;
            }

            Ray raycast = UnityEngine.Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            clearPopUps();
            if (nextRayAction != null) {
                var call = nextRayAction;
                nextRayAction = null;
                call.Invoke(raycast);
            }
            else {
                handleRay(raycast);
            }
        }
        
        RaycastHit hit;

        if (Input.GetMouseButtonDown (0)){
            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition),  out hit)){
                start = hit.point;
            }
        }

        if (Input.GetMouseButtonUp(0)) {
            
            Vector3 distance = Vector3.zero;
            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition),  out hit)){
                var end = hit.point;
                distance = end - start;
            }

            var sinceLast = Time.unscaledTime - lastClick;
            lastClick = Time.unscaledTime;
            
            if (overlayClicked || distance.magnitude > 1 || sinceLast < 0.15) {
                overlayClicked = false;
                return;
            }
            Ray raycast = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
            clearPopUps();
            if (nextRayAction != null) {
                var call = nextRayAction;
                nextRayAction = null;
                call.Invoke(raycast);
            }
            else {
                handleRay(raycast);
            }
        }
    
        if (outlines.Count > 0) {
            List<Outline> toRemove = new List<Outline>();
            foreach (Outline outline in outlines) {
                try {
                    outline.OutlineWidth -= Time.unscaledDeltaTime * 10f;
                    if (outline.OutlineWidth < 0.1f) {
                        Destroy(outline);
                        //outlines.Remove(outline);
                        toRemove.Add(outline);
                    }
                } catch (NullReferenceException ex) {
                    toRemove.Add(outline);
                }
            }
            foreach(Outline elem in toRemove) outlines.Remove(elem);
            toRemove.Clear();
        }

    }

    private void handleRay(Ray raycast) {

        RaycastHit raycastHit;
        int layerMask = LayerMask.GetMask("Clickable");
        //Debug.Log("UI Open: " + canvasController.UIOpen);

        if (Physics.Raycast(raycast, out raycastHit, 200.0f, layerMask)) {
            Debug.Log("hit layer: " + raycastHit.collider.gameObject.layer + " checking for: " + LayerMask.NameToLayer("Clickable"));
            //if (raycastHit.collider.gameObject.layer.Equals(LayerMask.NameToLayer("Clickable"))) {
            float pressTime = Time.unscaledTime - onStart;
            Debug.Log("clickable tag clicked, time pressed: " + pressTime);
            GameObject o;
            clickable target = (clickable)(o = raycastHit.transform.gameObject).GetComponent(typeof(clickable));
        
            createHighlight(o);

            if (nextAction != null) {
                var call = nextAction;
                nextAction = null;
                call.Invoke(raycastHit.transform.gameObject);
                return;
            }

            if (pressTime > 0.2f) {
                target.handleLongClick();
            } else {
                target.handleClick();
            }
        }

    }

    private void createHighlight(GameObject on) {
        print("creating highlight for: " + on.name);
    
        //destroy existing effect
        if (on.GetComponent<Outline>() != null) {
            print("destroyed existing outline");
            GameObject.Destroy(on.GetComponent<Outline>());
        }

        Outline effect = on.AddComponent<Outline>();
        effect.OutlineColor = Color.cyan;
        effect.OutlineWidth = 4.0f;
        outlines.Add(effect);
        print("done creating highlight");
    }

    public static void clearPopUps() {
        ClickOptions.clear();
    }

    public void setNextClickAction(Action<GameObject> onClick) {
        print("next click has been overridden");
        nextAction = onClick;
    }

    public void setNextRayClickAction(Action<Ray> onClick) {
        print("next ray click has been overridden");
        nextRayAction = onClick;
    }

    public void resetNextClick() {
        nextAction = null;
        nextRayAction = null;
    }
}
