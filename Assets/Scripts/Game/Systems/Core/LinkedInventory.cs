﻿using UnityEngine;

namespace Content {
    public class LinkedInventory : inventory {

        private inventory linkedTo;

        private void Start() {
            linkedTo = GameObject.Find("Spaceship").GetComponent<inventory>();
            this.newContent = linkedTo.newContent;
        }
    }
}
