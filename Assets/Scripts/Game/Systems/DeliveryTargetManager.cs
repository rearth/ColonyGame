﻿using System;
using System.Collections.Generic;
using System.Linq;
using Content;
using UnityEngine;

namespace Game.Systems {
	public class DeliveryTargetManager : MonoBehaviour {

		private Dictionary<inventory, targetAmountData> targetData = new Dictionary<inventory, targetAmountData>();
		private List<ActionController> allMovers = new List<ActionController>();

		private inventory baseInventory;

		private int counter = 30;
		private bool refreshTriggered = false;

		private void Start() {
			this.baseInventory = GameObject.FindGameObjectWithTag("dropBase").GetComponent<inventory>();
		}

		private void FixedUpdate() {

			counter++;
			if (counter % 600 == 0) {
				refreshMovers();
			}

			if (counter % 120 == 0 || refreshTriggered) {
				var avail = getAvailMovers(allMovers);
				if (avail.Count > 0 && targetData.Count > 0) {
					processData(avail);
				}

				refreshTriggered = false;
			}

			//collect available movers
			//gather elements with highest priority
			//process those elements, make priority low again
			//increase priority off all elements, those with high missing amount get even more

		}

		public void triggerRefresh() {
			refreshTriggered = true;
		}

		private void processData(List<ActionController> availMovers) {

			var invsByPriority = targetData.Keys.ToList();
			invsByPriority = invsByPriority.OrderBy(o => targetData[o].priority).ToList();
			updatePriorities();

			int prog = 0;
			foreach (var mover in availMovers) {
				for (int i = prog; i < invsByPriority.Count; i++) {
					var inv = invsByPriority[i];
					if (commandMover(mover, inv)) {
						prog++;
						targetData[inv].priority = 40;
						break;
					}
				}
			}

		}

		private bool commandMover(ActionController mover, inventory inv) {
		
			//check if ressources need to be removed
			//find out which ressources are required
			//check if ressources are available

			var data = targetData[inv];
			var toTake = new List<ressourceStack>();
			foreach (var resourceType in inv.newContent.Keys) {
				if (!listHasRessourceKind(resourceType, data.wants)) {
					if (inv.getAmount(resourceType) > 0) {
						toTake.Add(new ressourceStack(inv.getAmount(resourceType), resourceType));
					}
				}
			}

			if (toTake.Count > 0) {
				//deliverFromInvToBase
				pickupFromInv(toTake, mover, inv);
				return true;
			}

			var toBring = new List<ressourceStack>();
			foreach (var stack in data.wants) {
				if (!inv.canTake(stack) && baseInventory.canTake(stack)) {
					toBring.Add(stack);
				}
			}

			if (toBring.Count > 0) {
				//deliverListToInv
				deliverToInv(toBring, mover, inv);
				return true;
			}

			return false;

		}

		private void deliverToInv(List<ressourceStack> toBring, ActionController mover, inventory bringTo) {
			var dropBase = RessourceHelper.getClosestDropbase(mover.transform.position);
			EnergyHandler.Shuffle(toBring);
			mover.deliverTo(dropBase, bringTo.gameObject, toBring[0]);
		}

		private void pickupFromInv(List<ressourceStack> toTake, ActionController mover, inventory takeFrom) {
			var dropBase = RessourceHelper.getClosestDropbase(takeFrom.transform.position);
			EnergyHandler.Shuffle(toTake);
			mover.deliverTo(takeFrom.gameObject, dropBase, toTake[0]);
		}


		private void updatePriorities() {
			foreach (var targetAmountData in targetData) {
				//check if needs ressources
				targetAmountData.Value.priority++;

				var fillPercent = 0f;
				foreach (var stack in targetAmountData.Value.wants) {
					fillPercent += targetAmountData.Key.getFillPercent(stack.getRessource());

					if (!targetAmountData.Key.canTake(stack)) {
						targetAmountData.Value.priority += 3;
					}
					
					//check if ressource is available
					if (baseInventory.canTake(stack)) {
						targetAmountData.Value.priority += 3;
					}
					
				}

				if (fillPercent < targetAmountData.Value.targetFillAmount) {
					targetAmountData.Value.priority += 2;
				}
				
				//check for unwanted ressources in inventory
				foreach (var ressource in targetAmountData.Key.newContent.Keys) {
					if (!listHasRessourceKind(ressource, targetAmountData.Value.wants)) {
						if (targetAmountData.Key.getAmount(ressource) > 15) {
							targetAmountData.Value.priority += 4;
						}
						else if (targetAmountData.Key.getAmount(ressource) > 1) {
							targetAmountData.Value.priority += 1;
						}
					}
				}
				
			}
		}

		private bool listHasRessourceKind(ressources resource, List<ressourceStack> list) {
			foreach (var elem in list) {
				if (elem.getRessource().Equals(resource)) return true;
			}
			return false;
		}

		private String formatAll(List<inventory> sorted) {
			int i = 0;
			String res = "\n";
			foreach (var elem in sorted) {
				res += i++ + ": " + targetData[elem] + "|" + elem.gameObject.name + ";\n";
			}

			return res;

		}

		public void SetWantData(inventory inv, List<ressourceStack> wants, float targetFillAmount) {
			var data = new targetAmountData(wants, targetFillAmount, inv.maxSize);
			targetData[inv] = data;
			print("added new targetData");
		}

		public void clear(inventory inv) {
			targetData.Remove(inv);
		}

		private List<ActionController> getAvailMovers(List<ActionController> allMovers) {
			var res = new List<ActionController>();
			foreach (ActionController controller in allMovers) {

				//check if it is valid
				if (controller.getState().Equals(ActionController.State.Idle)) {
					print("mover state is valid: " + controller.getState() + ":" + controller.gameObject.name);
					res.Add(controller);
				}
			}

			return res;
		}

		private void refreshMovers() {
			var elems = GameObject.FindGameObjectsWithTag("mover");
			allMovers.Clear();
			foreach (var elem in elems) {
				allMovers.Add(elem.GetComponent<ActionController>());
			}
		}
	}

	class targetAmountData {
		internal List<ressourceStack> wants;
		internal float targetFillAmount;
		internal int priority = 50; //higher = better

		public targetAmountData(List<ressourceStack> wants, float targetFillAmount, float invSize) {
			
			//wantsSum = targetFillAmount * invSize
			//300 = 0.6 * 1000
			//300 = 600
			//-> elem * (600 / 300)
			
			//scale stacks to reflection fraction of total amount
			var wantsSize = wants.Sum(elem => elem.getAmount());
			var scale = invSize * targetFillAmount / wantsSize;

			var scaledList = new List<ressourceStack>();
			foreach (var elem in wants) {
				var copy = elem.clone();
				copy.setAmount(copy.getAmount() * scale);
				scaledList.Add(copy);
			}
			

			this.wants = scaledList;
			this.targetFillAmount = targetFillAmount;
		}

		public override string ToString() {
			return $"{nameof(wants)}: {wants}, {nameof(targetFillAmount)}: {targetFillAmount}, {nameof(priority)}: {priority}";
		}
	}
}
