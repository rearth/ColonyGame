﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class simpleClickable : SimpleStructure, clickable {

	public String description;
	
	public void handleClick() {
		if (Salvaging.isActive()) {
			Salvaging.createNotification(this.gameObject);
			return;
		}
	}

	public void handleLongClick() {
		this.gameObject.GetComponent<ClickOptions>().Create();
	}

	public void handleOption(string option) {
		displayInfo();
	}

	public void displayInfo() {
		InfoClicked controller = InfoClicked.getInstance();
		controller.show();
        
		controller.setTitle(this.gameObject.name);
		controller.setDesc(description);
	}

	public PopUpCanvas.popUpOption[] getOptions() {
		PopUpCanvas.popUpOption info = new  PopUpCanvas.popUpOption("Info", UIPrefabCache.InfoBut);
        
		var options = new[]{info};
		return options;
	}
}
