﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCreator : MonoBehaviour {
    public GameObject cancelBut;
    public GameObject cornerMarker;
    public GameObject wallPlacement;
    public GameObject menu;
    public GameObject wallConstruction;

    public ressourceStack[] pillarCost;
    public ressourceStack[] wallBaseCost; //base cost for 10m of wall

    private readonly List<Vector3> markers = new List<Vector3>();
    private readonly List<Vector3> blockedMarkers = new List<Vector3>();
    private readonly List<Vector3> extraMarkers = new List<Vector3>();
    private readonly List<GameObject> placements = new List<GameObject>();

    private bool buildWall = false;
    private bool buildBridge = false;
    private bool buildSurface = false;

    public void createWallPressed() {
        clickDetector.overlayClicked = true;
        Scene_Controller.getInstance().hideAllUI();
        cancelBut.SetActive(true);
        menu.SetActive(true);
        TimeScaleHandler.setScale(0.1f);
    }

    public void wallSelected() {
        menu.SetActive(false);
        clickDetector.overlayClicked = true;
        Debug.Log("starting wall creation");

        selectPoint();
        buildWall = true;
    }

    public void bridgeSelected() {
        Debug.Log("starting wall creation");
        menu.SetActive(false);
        clickDetector.overlayClicked = true;

        selectPoint();
        buildBridge = true;
    }

    public void surfaceSelected() {
        Debug.Log("starting surface selection");
        menu.SetActive(false);
        clickDetector.overlayClicked = true;

        selectPoint();
        buildSurface = true;
    }

    private void selectPoint() {
        GameObject.Find("Main Camera").GetComponent<clickDetector>().setNextRayClickAction(handleRay);
    }

    private void handleRay(Ray ray) {
        print("handling ray for wall creation: " + ray);
        RaycastHit raycastHit;

        if (!Physics.Raycast(ray, out raycastHit, 200.0f)) {
            print("raycast returned invalid!");
            return;
        }

        var hitPos = raycastHit.point;
        onPointSelected(hitPos);
    }

    private void onPointSelected(Vector3 point) {
        print("creating wall marker at: " + point);
        //create placements
        var marker = GameObject.Instantiate(cornerMarker, point, Quaternion.identity);

        if (markers.Count >= 1) {
            if (buildWall) {
                createWall(markers[markers.Count - 1], point, true);
            }
            else if (buildBridge) {
                markers.Add(point);
                placements.Add(marker);
                finishBridgeBuilding(markers[0], markers[1]);
                return;
            }
        }

        markers.Add(point);
        placements.Add(marker);
        selectPoint();
    }

    public void cancelPressed() {
        clickDetector.overlayClicked = true;
        restoreNormal();
    }

    private void finishBridgeBuilding(Vector3 fromPos, Vector3 toPos) {
        var dir = Quaternion.LookRotation(toPos - fromPos);
        var length = (fromPos - toPos).magnitude;
        print("creating bridge with length: " + length + " from=" + fromPos + " to=" + toPos);

        //instantiate bridge
        ressourceStack[] calcedCost = new ressourceStack[wallBaseCost.Length];
        for (int i = 0; i < wallBaseCost.Length; i++) {
            calcedCost[i] = new ressourceStack(wallBaseCost[i].getAmount() * length / 10f, wallBaseCost[i].getRessource());
        }

        var bridgeObj = GameObject.Instantiate(wallConstruction, fromPos, dir);
        bridgeObj.GetComponent<WallConstruction>().setData(fromPos, toPos, calcedCost, true);
        bridgeObj.transform.Rotate(new Vector3(-90, 0, 0));

        restoreNormal();
    }

    public void finishWallbuilding() {
        clickDetector.overlayClicked = true;
        checkClosePoints();
        createWalls();
        restoreNormal();
    }

    private void createWalls() {
        if (markers.Count == 0) {
            return;
        }

        for (int i = 1; i < markers.Count; i++) {
            createWall(markers[i - 1], markers[i]);
        }

        foreach (var pillar in markers) {
            if (blockedMarkers.Contains(pillar)) continue;
            var obj = GameObject.Instantiate(wallConstruction, pillar, Quaternion.identity);
            obj.GetComponent<WallConstruction>().setData(pillar, pillarCost);
            obj.transform.Rotate(new Vector3(-90, 0, 0));
        }

        foreach (var pillar in extraMarkers) {
            var obj = GameObject.Instantiate(wallConstruction, pillar, Quaternion.identity);
            obj.GetComponent<WallConstruction>().setData(pillar, pillarCost);
            obj.transform.Rotate(new Vector3(-90, 0, 0));
        }
    }

    private void createWall(Vector3 fromPos, Vector3 toPos, bool placement = false) {
        var middle = fromPos + (toPos - fromPos) / 2;
        var dir = Quaternion.LookRotation(toPos - fromPos);
        var length = (fromPos - toPos).magnitude;
        print("creating wall with length: " + length + " from=" + fromPos + " to=" + toPos);

        //instantiate wall, and scale it
        GameObject wallObj;
        if (placement) {
            wallObj = GameObject.Instantiate(wallPlacement, middle, dir);
            placements.Add(wallObj);
            wallObj.transform.Rotate(new Vector3(1, -90, 1));
            wallObj.transform.localScale = new Vector3(length, 3, 1);
        }
        else {
            foreach (var cost in wallBaseCost) {
                cost.setAmount(cost.getAmount() * length / 10f);
            }

            wallObj = GameObject.Instantiate(wallConstruction, middle, dir);
            wallObj.GetComponent<WallConstruction>().setData(fromPos, toPos, wallBaseCost, false);
            wallObj.transform.Rotate(new Vector3(-90, 0, 0));
        }
    }

    private void checkClosePoints() {
        //check if 2 points are within 2 meters, and add them to blocked List if true
        foreach (var source in markers) {
            foreach (var target in markers) {
                if (Vector3.Distance(source, target) < 2f) {
                    blockedMarkers.Add(source);
                    blockedMarkers.Add(target);
                    markers[markers.IndexOf(target)] = source;
                    if (!extraMarkers.Contains(source) && !extraMarkers.Contains(target)) {
                        extraMarkers.Add(source);
                    }
                }
            }
        }
    }

    private void restoreNormal() {
        TimeScaleHandler.normalizeScale();
        Scene_Controller.getInstance().restoreDefaultUI();
        cancelBut.SetActive(false);
        GameObject.Find("Main Camera").GetComponent<clickDetector>().resetNextClick();
        foreach (var marker in placements) {
            Destroy(marker);
        }

        placements.Clear();
        blockedMarkers.Clear();
        extraMarkers.Clear();
        markers.Clear();
        buildWall = false;
        buildBridge = false;
        buildSurface = false;
    }
}