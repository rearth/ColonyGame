﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QualitySettingsMenu : MonoBehaviour {
	private void Start() {
		var level = PlayerPrefs.GetInt("qualityLevel");
		QualitySettings.SetQualityLevel(level, true);
	}

	//0-4, from low to ultra
	public void setQualityLevel(int level) {
		print("updating quality level to: " + level);
		QualitySettings.SetQualityLevel(level, true);
		PlayerPrefs.SetInt("qualityLevel", level);
	}
}
