﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyStorageCluster : DefaultStructure {
    public override int getMaxEnergy() {
        return 15000;
    }

    public override int getMaxOutput() {
        return 1000;
    }

    public override int getMaxInput() {
        return 1500;
    }

    public override int getPriority() {
        return 8;
    }

    public override PopUpCanvas.popUpOption[] getOptions() {
        PopUpCanvas.popUpOption info = new PopUpCanvas.popUpOption("Info", infoBut);
        return new PopUpCanvas.popUpOption[] {info};
    }

    public override void handleOption(string option) {
        Debug.Log("handling option: " + option);

        if (salvaging) {
            return;
        }

        switch (option) {
            default:
                displayInfo();
                break;
        }
    }
}